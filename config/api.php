<?php

return [
    'subway_domain' => env('SUBWAY_DOMAIN', 'https://subway-local.dev'),
    'provider_id' => env('PROVIDER_ID'),
    'stripe_secret' => env('STRIPE_SECRET'),
    'phoenix_stripe_id' => env('PHOENIX_STRIPE_ID'), //消す
    'phoenix_stripe_coupon' => env('PHOENIX_STRIPE_COUPON'),//消す
    'phoenix_plan' => env('PHOENIX_PLAN'),
    'phoenix_first_month_coupon' => env('PHOENIX_FIRST_MONTH_COUPON')
];
