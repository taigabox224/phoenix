@extends('layouts.app')

{{--@section('content')--}}
{{--    <div class="container">--}}
{{--        <div class="row justify-content-center">--}}
{{--            <div class="col-md-8">--}}
{{--                @component('components.session_alert')--}}
{{--                @endcomponent--}}
{{--                <div class="card">--}}
{{--                    <div class="card-header">Payment</div>--}}
{{--                    <div class="card-body">--}}
{{--                        <h1>フェニックス本登録</h1>--}}

{{--                        --}}{{-- サブスクリプション購読中かつキャンセル処理中でない場合 --}}
{{--                        @if(\Auth::user()->isPaidByCard() && !\Auth::user()->subscription('default')->cancelled())--}}
{{--                            <form action="{{ route('payment.cancel') }}" method="post" name="cancel">--}}
{{--                                @csrf--}}
{{--                                <input type="submit" value="フェニックスコースを退会する"--}}
{{--                                       onclick="deleteAlert('cancel');return false">--}}
{{--                            </form>--}}
{{--                            <p id="currentNumberLastFour">現在の支払い方法の末尾4桁:{{ \Auth::user()->card_last_four }}</>--}}
{{--                            <p>変更する場合は下記フォームを再入力してください</p>--}}

{{--                        @endif--}}

{{--                        --}}{{-- キャンセル後の猶予期間中の場合 --}}
{{--                        @if(\Auth::user()->isPaidByCard() && \Auth::user()->subscription('default')->onGracePeriod())--}}
{{--                            <p>ご利用ありがとうございました。</p>--}}
{{--                            <p>{{ date('Y年m月d日', strtotime(\Auth::user()->subscriptions()->first()['ends_at'])) }}--}}
{{--                                までコンテンツを利用できます</p>--}}
{{--                        @else--}}
{{--                            カード名義人<input id="card-holder-name" type="text">--}}

{{--                            <!-- Stripe Elements Placeholder -->--}}
{{--                            <div id="card-element"></div>--}}

{{--                            <button class="btn btn-primary mt-3" id="card-button" data-secret="{{ $intent->client_secret }}">--}}
{{--                                登録する--}}
{{--                            </button>--}}
{{--                            <p id="message" style="color:red;"></p>--}}

{{--                            --}}{{-- 初期登録ならsubscribeメソッドで更新ならばupdateメソッドを呼ぶ --}}
{{--                            <form--}}
{{--                                action="@if(\Auth::user()->hasDefaultPaymentMethod()) {{ route('payment.update') }} @else {{ route('payment.subscribe') }} @endif"--}}
{{--                                method="post" name="submit">--}}
{{--                                @csrf--}}
{{--                                <input hidden="text" class="form-control" id="paymentMethod" name="payment_method">--}}
{{--                                <input type="submit" value="送信">--}}
{{--                            </form>--}}
{{--                        @endif--}}

{{--                        <script src="https://js.stripe.com/v3/"></script>--}}

{{--                        <script>--}}
{{--                            const stripe = Stripe('pk_live_Mx7sARDHvU6nEqFWVq73scWS00oHcwU7Hs');--}}

{{--                            const elements = stripe.elements();--}}
{{--                            const cardElement = elements.create('card');--}}

{{--                            cardElement.mount('#card-element');--}}

{{--                            const cardHolderName = document.getElementById('card-holder-name');--}}
{{--                            const cardButton = document.getElementById('card-button');--}}
{{--                            const clientSecret = cardButton.dataset.secret;--}}
{{--                            const paymentMethod = document.getElementById('paymentMethod');--}}
{{--                            const message = document.getElementById('message');--}}

{{--                            cardButton.addEventListener('click', async (e) => {--}}
{{--                                const {setupIntent, error} = await stripe.confirmCardSetup(--}}
{{--                                    clientSecret, {--}}
{{--                                        payment_method: {--}}
{{--                                            card: cardElement,--}}
{{--                                            billing_details: {name: cardHolderName.value}--}}
{{--                                        }--}}
{{--                                    }--}}
{{--                                );--}}

{{--                                if (error) {--}}
{{--                                    message.innerHTML = 'このクレジットカードはご利用いただけません';--}}
{{--                                } else {--}}
{{--                                    paymentMethod.value = setupIntent.payment_method;--}}
{{--                                    document.forms['submit'].submit();--}}
{{--                                }--}}
{{--                            });--}}

{{--                            deleteAlert = name => {--}}
{{--                                if (!window.confirm('本当に退会してよろしいですか？')) {--}}
{{--                                    window.alert('キャンセルされました');--}}
{{--                                    return false;--}}
{{--                                }--}}
{{--                                document.forms[name].submit();--}}
{{--                            }--}}
{{--                        </script>--}}

{{--                        <p>初月(30日)　9,800円(+税)<br>次月以降　  29,900円(+税)</p>--}}

{{--                        <p>【注意事項】</p>--}}

{{--                        <p>※羅生門・Galaxyにご参加された方は3ヶ月無料になります。岡田公式LINEにご連絡ください。</p>--}}

{{--                        <p>※JCBに関してはメールにてお送りした専用フォームからお手続してください。</p>--}}

{{--                        <p>※銀行振込をご希望の方は、自動返信メールをご覧ください。</p>--}}

{{--                        <p>※再度登録をしていただくと次回の決済のカードが更新されます。</p>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--@endsection--}}
