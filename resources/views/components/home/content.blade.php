<li class="content-item">
    <img src="{{ asset('images/content_image_' . $i .'.png') }}">
    <div class="content-action-menu">
        <form action="{{ route('home.download') }}" method="post"
              class="content-download-form">
            @csrf
            <input type="hidden" name="content_url" value="{{ $data[$i]['content_url'] }}">
            <button type="submit" class="content-action-download-active">Download</button>
        </form>

        <div class="content-action-image"><img src="{{ asset('images/open.svg') }}"
                                               alt="open">
        </div>
        <div class="content-action-count"><p>公開中</p></div>
    </div>
</li>
