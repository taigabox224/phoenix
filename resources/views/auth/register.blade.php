@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">フェニックス会員登録フォーム</div>

                    <div class="card-body">
                        <p>初月 9,800円+税<br>次月 14,800円+税</p>

                        <p>※一ヶ月=決済開始から30日間<br>※こちらを登録された時点では決済は行われません。</p>

                        <form method="POST" action="{{ route('register') }}" name="register">
                            @csrf

                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">お名前</label>

                                <div class="col-md-6">
                                    <input id="name" type="text"
                                           class="form-control @error('name') is-invalid @enderror" name="name"
                                           value="{{ old('name') }}" required autocomplete="name" autofocus>

                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="line_name" class="col-md-4 col-form-label text-md-right">LINE名</label>

                                <div class="col-md-6">
                                    <input id="line_name" type="text"
                                           class="form-control @error('line_name') is-invalid @enderror"
                                           name="line_name" value="{{ old('line_name') }}" required autocomplete="name"
                                           autofocus>

                                    @error('line_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="email" class="col-md-4 col-form-label text-md-right">Eメール</label>

                                <div class="col-md-6">
                                    <input id="email" type="email"
                                           class="form-control @error('email') is-invalid @enderror" name="email"
                                           value="{{ old('email') }}" required autocomplete="email">

                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                    <p>※こちらが<u>会員IDになります</u>。 また、ご連絡のメッセージもこちらにお送りいたしますので、<span style="color:red;"><u>必ず受信可能なアドレスをご記入してください。</u></span>
                                    </p>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="password" class="col-md-4 col-form-label text-md-right">パスワード</label>

                                <div class="col-md-6">
                                    <input id="password" type="password"
                                           class="form-control @error('password') is-invalid @enderror" name="password"
                                           required autocomplete="new-password">

                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="password-confirm"
                                       class="col-md-4 col-form-label text-md-right">パスワード(確認用)</label>

                                <div class="col-md-6">
                                    <input id="password-confirm" type="password" class="form-control"
                                           name="password_confirmation" required autocomplete="new-password">
                                </div>
                            </div>


                            <div class="form-check">
                                <input type="checkbox" class="form-check-input" id="agreement" name="agreement">
                                <label class="form-check-label" for="agreement"><u>注意事項に同意しました</u></label>
                            </div>


                            <script>
                                checkAlert = name => {
                                    const checked = document.forms['register'].agreement.checked;
                                    if (!checked) {
                                        alert('注意事項の同意にチェックを入れてください');
                                        return false;

                                    }
                                    document.forms[name].submit();
                                }
                            </script>


                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button class="btn btn-primary mt-3" onclick="checkAlert('register');return false">
                                        会員登録
                                    </button>
                                </div>
                            </div>

                            <p class="mt-3" style="color:red;"><u>登録後にご参加者様のメールアドレス宛に決済方法のご案内が届きますので必ずご確認ください。</u></p>
                            <p>【注意事項】<br>本サービスは運用した際の利益を保証するものではない事をご理解ください。サービスの関係上、退会申請前の決済に関してはご返金ができません。運営側が提供するサービス等はご利用者様がコミュニティにご参加中に限りご利用可能です。
                            </p>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
