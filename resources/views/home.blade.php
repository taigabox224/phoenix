{{--@extends('layouts.app')--}}

<head>
    <link href="{{ asset('css/reset.css') }}" rel="stylesheet">
    <link href="{{ asset('css/home.css') }}" rel="stylesheet">
</head>

<body>
<div id="wrapper">
    {{--  header  --}}
    <header class="header">
        <div class="title">
            <h1><a href="#">PHOENIX</a></h1>
        </div>
        <nav class="g-nav">
            <ul class="g-nav-menu">
                <a href="#content">
                    <li>コンテンツ一覧</li>
                </a>
                <a href="#document">
                    <li>各種マニュアル一覧</li>
                </a>
            </ul>
        </nav>
    </header>

    {{--  main  --}}

    <div class="hero">
        <img src="{{ asset('images/hero.jpg') }}" alt="hero">
    </div>
    <div class="main">

        @if(\Auth::user()->isPaid())
            <section class="content" id="content">
                <h2 class="content-title">コンテンツ一覧</h2>
                <ul class="content-list">

                    @for($i=0; $i < count($data); $i++)
                        @component('components.home.content', ['data' => $data,'i' => $i])
                        @endcomponent
                    @endfor

                    @for($i=0; $i < 6 - count($data); $i++)
                        <li class="content-item">
                            <img src="{{ asset('images/coming.jpg') }}">
                            <div class="content-action-menu">
                                <button class="content-action-download">Download</button>
                                <div class="content-action-image"><img src="{{ asset('images/locked.svg') }}"
                                                                       alt="locked">
                                </div>
                                <div class="content-action-count"><p>近日公開</p></div>
                            </div>
                        </li>
                    @endfor
                </ul>
            </section>

            <section class="document" id="document">
                <h2>各種アニュアル一覧</h2>
                <ul class="document-list">
                    <li class="document-item">
                        <a href="https://www.chatwork.com/#!rid192780800-1325903428970389504">
                            <button class="document-button">❶　運用手順について</button>
                        </a>
                    </li>
                    <li class="document-item">
                        <a href="https://binary-option.biz/wordpress/wp-content/uploads/2020/06/XM資料フェニックス用3.pdf">
                            <button class="document-button">❷　口座解説マニュアル</button>
                        </a>
                    </li>
                    <li class="document-item">
                        <a href="https://binary-option.biz/wordpress/wp-content/uploads/2020/06/MT4設定資料フェニックス.pdf">
                            <button class="document-button">❸ MT4設定マニュアル</button>
                        </a>
                    </li>
                    <li class="document-item">
                        <a href="https://binary-option.biz/wordpress/wp-content/uploads/2020/06/VPS設定方法お名前.pdf">
                            <button class="document-button">❹　VPS設定マニュアル</button>
                        </a>
                    </li>
                    <li class="document-item">
                        <a href="https://docs.google.com/forms/d/e/1FAIpQLSd96yLwlrt5_V0XGUE25lDLB4jyqSJ1QREG57abTLUpY4dYIQ/viewform">
                            <button class="document-button">❺ ご利用申請フォーム</button>
                        </a>
                    </li>
                    <li class="document-item">
                        <a href="https://docs.google.com/forms/d/e/1FAIpQLScE-sowCCAHiju2wIasOsPjMSFNqK58vmFv-P4kuUYgbm2Jvg/viewform">
                            <button class="document-button">❻ 遠隔操作申請フォーム</button>
                        </a>
                    </li>
                </ul>
                <div class="document-support">
                    <p>その他のご不明点は<a href="https://lin.ee/egYjE9V"><span><u>専用サポートLINE</u></span></a>までご連絡ください</p>
                </div>
            </section>
        @else
            <p>決済確認が取れ次第、コンテンツをご利用いただけます</p>
        @endif


    </div>
    <footer>
        <div><p><a href="https://binary-option.biz/NEO/tokuteio.html">特定商取引法について</a>｜<a href="https://binary-option.biz/menseki/">免責事項</a></p></div>
    </footer>
</div>
</body>
