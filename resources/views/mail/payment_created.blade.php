<p>登録が完了いたしました</p>


<p>この度はフェニックスへのご参加、非常に嬉しく思います。</p>

<p>引き続きご案内させていただきます。</p>


<p>まずは、こちらのチャットワークに利用申請をよろしくお願いします。</p>

<p><a href="https://www.chatwork.com/g/pxeszuakyd8i9j">https://www.chatwork.com/g/pxeszuakyd8i9j</a></p>
<p>※必ずご登録時の本名で申請してください。</p>
<p>確認出来ない場合や変更された場合は、ご登録ができません。</p><br>


<p>申請が完了しましたらそのままお待ち頂くか、岡田公式LINEにご連絡をお願いいたします。</p>

<p><a href="https://lin.ee/2jNNEh7">https://lin.ee/2jNNEh7</a></p><br>


<p>また、こちらがフェニックスサポート専用LINEになります。</p>

<p><a href="https://lin.ee/egYjE9V">https://lin.ee/egYjE9V</a></p><br>


<p>基本的にはチャットワーク上に情報をアップしてありますが、何か解らない点などありましたらサポート専用LINEにお問い合わせください。</p>
<p><a href="https://www.chatwork.com/g/pxeszuakyd8i9j">https://www.chatwork.com/g/pxeszuakyd8i9j
    </a></p><br>
