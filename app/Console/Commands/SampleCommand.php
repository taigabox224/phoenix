<?php

namespace App\Console\Commands;

use App\Mail\UnpaidNotificationMail;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use App\Models\User;


class SampleCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:sample';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $user = User::all()[0];
        Mail::to('taigabox224@gmail.com')->send(new UnpaidNotificationMail($user));
    }
}
