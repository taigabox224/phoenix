<?php

namespace App\Console\Commands;

use App\Mail\PaymentCreatedMail;
use Carbon\Carbon;
use Illuminate\Console\Command;
use App\Models\User;
use App\Mail\UnpaidNotificationMail;
use Illuminate\Support\Facades\Mail;

class SendUnpaidMail extends Command
{
    const PAYMENT_BANK = 1;
    const PAYMENT_TAKETIN = 2;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:send_unpaid_mail';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '未払いメールの送信';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param int $elapsed_date
     * @return bool
     */
    private function canSendUnpaidMailToBank(int $elapsed_date)
    {
        return $elapsed_date % 31 === 0 || $elapsed_date % 32 === 0 || $elapsed_date % 33 === 0 || $elapsed_date % 34 === 0 || $elapsed_date % 35 === 0;
    }

    /**
     * @param int $elapsed_date
     * @return bool
     */
    private function canSendUnpaidMailToTAKETIN(int $elapsed_date)
    {
        return $elapsed_date % 33 === 0 || $elapsed_date % 34 === 0 || $elapsed_date % 35 === 0;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $users = User::all();
        $current_time = new Carbon((new Carbon())->format('Y-m-d'));

        foreach ($users as $user) {
            // サービス開始していないユーザーは除外する
            if ($user->service_start_date === NULL) continue;
            // 既に支払済・前払済み・停止済みのユーザーは除外する
            if ($user->payment_status_bank !== 0 && $user->payment_status_bank !== 2) continue;
            switch (TRUE) {
                // 銀行振込の場合
                case $user->payment === self::PAYMENT_BANK :
                    $service_start_date = new Carbon((new Carbon($user->service_start_date))->format('Y-m-d'));
                    $elapsed_date = $service_start_date->diffInDays($current_time);
                    // 31日経過ごとにステータスの更新を行う
                    if ($this->canSendUnpaidMailToBank($elapsed_date)) {
                        Mail::to($user->email)->send(new UnpaidNotificationMail($user));
                        $this->info($user->id . 'に未払い報告メールを送信します');
                    }

                    break;
                // TAKETIN払いの場合
                case $user->payment === self::PAYMENT_TAKETIN :
                    $service_start_date = new Carbon((new Carbon($user->service_start_date))->format('Y-m-d'));
                    $elapsed_date = $service_start_date->diffInDays($current_time);
                    // 33日経過ごとにステータスの更新を行う
                    if ($this->canSendUnpaidMailToTAKETIN($elapsed_date)) {
                        Mail::to($user->email)->send(new UnpaidNotificationMail($user));
                        $this->info($user->id . 'に未払い報告メールを送信します');
                    }
                    break;
            }
        }
    }
}
