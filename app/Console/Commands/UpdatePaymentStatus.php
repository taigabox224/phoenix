<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use App\Models\User;

class UpdatePaymentStatus extends Command
{
    const PAYMENT_BANK = 1;
    const PAYMENT_TAKETIN = 2;

    // 2 未払い（継続）
    // 3 前払い済
    // 4 停止


    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:update_payment_status';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '支払いステータスを更新する';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $users = User::all();
        $current_time = new Carbon((new Carbon())->format('Y-m-d'));

        foreach ($users as $user) {
            // サービス開始していないユーザーは除外する
            if ($user->service_start_date === NULL) continue;
            switch (TRUE) {
                // 銀行振込の場合
                case $user->payment === self::PAYMENT_BANK :
                    $service_start_date = new Carbon((new Carbon($user->service_start_date))->format('Y-m-d'));
                    $elapsed_date = $service_start_date->diffInDays($current_time);
                    // 31日経過ごとにステータスの更新を行う
                    if ($elapsed_date % 31 === 0) {
                        switch (TRUE) {
                            // 前払い済みステータスの場合は支払済ステータスへ
                            case $user->payment_status_bank === 3 :
                                $user->update(['payment_status_bank' => 1]);
                                $this->info($user->id . 'の支払いステータスを支払済に更新しました');
                                break;
                            // 支払済ステータスの場合は未払い（継続）ステータスへ
                            case $user->payment_status_bank === 1 :
                                $user->update(['payment_status_bank' => 2]);
                                $this->info($user->id . 'の支払いステータスを未払い（継続）に更新しました');
                                break;
                        }
                    } else if ($elapsed_date % 36 === 0) {
                        if ($user->payment_status_bank !== 3 && $user->payment_status_bank !== 1) {
                            $user->update(['payment_status_bank' => 4]);
                            $this->info($user->id . 'の支払いステータスを停止に更新しました');
                        }
                    }

                    break;
                // TAKETIN払いの場合
                case $user->payment === self::PAYMENT_TAKETIN :
                    $service_start_date = new Carbon((new Carbon($user->service_start_date))->format('Y-m-d'));
                    $elapsed_date = $service_start_date->diffInDays($current_time);
                    // 33日経過ごとにステータスの更新を行う
                    if ($elapsed_date % 33 === 0) {
                        switch (TRUE) {
                            // 前払い済みステータスの場合は支払済ステータスへ
                            case $user->payment_status_bank === 3 :
                                $user->update(['payment_status_bank' => 1]);
                                $this->info($user->id . 'の支払いステータスを支払済に更新しました');
                                break;
                            // 支払済ステータスの場合は未払い（継続）ステータスへ
                            case $user->payment_status_bank === 1 :
                                $user->update(['payment_status_bank' => 2]);
                                $this->info($user->id . 'の支払いステータスを未払い（継続）に更新しました');
                                break;
                        }
                    } else if ($elapsed_date % 36 === 0) {
                        if ($user->payment_status_bank !== 3 && $user->payment_status_bank !== 1) {
                            $user->update(['payment_status_bank' => 4]);
                            $this->info($user->id . 'の支払いステータスを停止に更新しました');
                        }
                    }
                    break;
            }
        }
    }
}
