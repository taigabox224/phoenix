<?php

namespace App\Repositories\Content;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

class ContentAPI
{
    /**
     * @param object $user
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function all(object $user): array
    {
        try {
            $client = new Client(
                [\GuzzleHttp\RequestOptions::VERIFY => FALSE]
            );

            $jsonCustomers = $client->request(
                'POST',
                config('api.subway_domain') . "/api/content/collection",
                [
                    'form_params' => [
                        'user_id' => config('api.provider_id'),
                        'service_start_date' => $user->service_start_date,
                        'bonus_point' => $user->bonus_point,
                        'subway_api_key' => '59a578fb-7d96-4b5f-ac55-fde99104ca80'
                    ]
                ]
            );
            return json_decode($jsonCustomers->getBody()->getContents(), TRUE);
        } catch (ClientException $e) {
            return ['data' => []];
        }
    }
}
