<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class User extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'line_name' => $this->line_name,
            'email' => $this->email,
            'payment' => $this->payment,
            'payment_status_bank' => $this->payment_status_bank,
            'payment_status_card' => $this->subscribed(),
            'bonus_point' => $this->bonus_point,
            'marks' => $this->marks,
            'service_start_date' => $this->service_start_date,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
