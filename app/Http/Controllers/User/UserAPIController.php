<?php

namespace App\Http\Controllers\User;

use App\Mail\PaymentCreatedMail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\User as UserResource;
use App\Models\User;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Support\Facades\Mail;

/**
 * Class UserAPIController
 * @package App\Http\Controllers\User
 */
class UserAPIController extends Controller
{
    /**
     * @param Request $request
     * @return UserResource
     */
    public function user(Request $request)
    {
        $user = User::find($request->id);
        return new UserResource($user);
    }

    /**
     * @param Request $request
     * @return AnonymousResourceCollection
     */
    public function userSearch(Request $request): AnonymousResourceCollection
    {
        $inputs = $request->input('inputs');

        //TODO::Userモデルに依存しているので切り分けする
        $users = User::all();

        $filtered = $users->when(\Arr::has($inputs, 'name'), function ($users) use ($inputs) {
            //　顧客名フィルター
            return $users->filter(function ($user) use ($inputs) {
                return strpos($user->name, $inputs['name']) !== FALSE;
            });
        })
            ->when(\Arr::has($inputs, 'line_name'), function ($users) use ($inputs) {
                //　顧客名フィルター
                return $users->filter(function ($user) use ($inputs) {
                    return strpos($user->line_name, $inputs['line_name']) !== FALSE;
                });
            })

            ->when(\Arr::has($inputs, 'email'), function ($users) use ($inputs) {
                //　メールフィルター
                return $users->filter(function ($user) use ($inputs) {
                    return strpos($user->email, $inputs['email']) !== FALSE;
                });
            })
            ->when(\Arr::has($inputs, 'payment'), function ($users) use ($inputs) {
                // 支払い方法フィルター
                return $users->where('payment', $inputs['payment']);
            })
            ->when(\Arr::has($inputs, ['bonus_point_operator', 'bonus_point']), function ($users) use ($inputs) {
                // ボーナスフィルター
                return $users->where('bonus_point', $inputs['bonus_point_operator'], $inputs['bonus_point']);
            })
            ->when(\Arr::has($inputs, 'payment_status'), function ($users) use ($inputs) {
                    return $users->where('payment_status_bank', $inputs['payment_status']);
            })
            ->when(\Arr::has($inputs, 'service_start_date'), function($users) use ($inputs){
                return $users->filter(function ($user) use ($inputs) {
                    return strpos($user->service_start_date, $inputs['service_start_date']) !== FALSE;
                });
            })
            ->when(\Arr::has($inputs, 'sort_service_start_date'), function ($users) use ($inputs) {
                switch ($inputs['sort_service_start_date']) {
                    case 'asc':
                        $users = $users->sortBy('service_start_date');
                        break;
                    case 'desc':
                        $users = $users->sortByDesc('service_start_date');
                        break;
                }
                return $users;
            });

        return UserResource::collection($filtered);
    }

    /**
     * @param User $userModel
     * @return AnonymousResourceCollection
     */
    public function userCollection(User $userModel): AnonymousResourceCollection
    {
        return UserResource::collection($userModel->all()->sortByDesc('service_start_date'));
    }

    /**
     * @param Request $request
     * @param User $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, User $user)
    {

        $inputs = $request->input('inputs');
        $user = $user->find($inputs['id']);

        \Log::debug($request);
        if($inputs['service_start_date'] != $user->service_start_date)
        {
            \Log::debug("OK");
            Mail::to($user->email)->send(new PaymentCreatedMail());
        }

        $res = $user->fill($inputs)->save();
        if ($res) {
            return response()->json([
                'status_code' => 200,
                'content' => 'Record is updated.',
                'message_type' => 'success'
            ]);
        }
        return response()->json([
            'status_code' => 500,
            'content' => 'failed to save.',
            'message_type' => 'danger'
        ]);
    }

    /**
     * @param Request $request
     * @param User $user
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy(Request $request, User $user)
    {
        \Log::debug($request);

        $id = $request->input('id');
        $res = $user->find($id)->delete();
        if ($res) {
            return response()->json([
                'status_code' => 200,
                'content' => 'Record is deleted.',
                'message_type' => 'success'
            ]);
        }
        return response()->json([
            'status_code' => 500,
            'content' => 'failed to delete.',
            'message_type' => 'danger'
        ]);
    }

    public function getArchivedList(User $userModel)
    {
        return UserResource::collection($userModel->onlyTrashed()->whereNotNull('id')->get()->sortByDesc('service_start_date'));
    }
}
