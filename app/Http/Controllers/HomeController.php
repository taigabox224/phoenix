<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Content\ContentAPI;
use Illuminate\Support\Facades\Storage;

class HomeController extends Controller
{
    private $api;

    /**
     * HomeController constructor.
     * @param ContentAPI $api
     */
    public function __construct(ContentAPI $api)
    {
        $this->middleware('auth');
        $this->api = $api;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function index()
    {
        $user = \Auth::user();
        if (!$user->isPaid()) return redirect()->away('https://t-mp.net/advc/subscribe/oRhxio');
        $data = $this->api->all($user)['data'];
        return view('home', ['data' => $data]);
    }

    public function download(Request $request)
    {
        $filePath = $request->content_url;
        $fileName = basename($request->content_url);
        $mimeType = Storage::mimeType($filePath);
        $headers = [['Content-Type' => $mimeType]];

        return Storage::download($filePath, $fileName, $headers);
    }
}
