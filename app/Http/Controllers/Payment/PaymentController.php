<?php

namespace App\Http\Controllers\Payment;

use App\Http\Controllers\Controller;
use App\Mail\UserCreatedMail;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Stripe\Stripe;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Mail\PaymentCreatedMail;

/**
 * Class PaymentController
 * @package App\Http\Controllers\Payment
 * @property User $loginUser
 */
class PaymentController extends Controller
{
    /**
     * @var \Illuminate\Contracts\Auth\Authenticatable|null
     */
    private $loginUser;

    /**
     * PaymentController constructor.
     */
    public function __construct()
    {
        Stripe::setApiKey(config('api.stripe_secret'));
        $this->middleware(function ($request, $next) {
            $this->loginUser = Auth::user();
            return $next($request);
        });
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('payments.index', [
            'intent' => $this->loginUser->createSetupIntent()
        ]);
    }

    /**
     * @param Request $request
     * @param User $user
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Throwable
     */
    public function subscribe(Request $request, User $user)
    {
        $inputs = $request->all();
        try {
            \DB::transaction(function () use ($inputs) {
                $this->loginUser->newSubscription('default', config('api.phoenix_plan'))->withCoupon(config('api.phoenix_first_month_coupon'))->create($inputs['payment_method']);
                $this->loginUser->update(['service_start_date' => Carbon::now()]);
            });
            Mail::to($this->loginUser->email)->send(new PaymentCreatedMail());
            return redirect(route('home.index'))->with('success', __('messages.success', ['target' => 'お支払い方法の新規登録']));
        } catch (\Exception $e) {
            return redirect(route('home.index'))->with('failure', __('messages.failure', ['target' => 'お支払い方法の新規登録']));
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|string
     */
    public function update(Request $request)
    {
        $inputs = $request->all();
        $this->loginUser->updateDefaultPaymentMethod($inputs['payment_method']);
        return redirect(route('payment.index'))->with('success', __('messages.success', ['target' => 'お支払い方法の更新']));

    }

    public function cancel(Request $request)
    {
        $this->loginUser->subscription('default')->cancel();
        return redirect(route('payment.index'))->with('success', __('messages.success', ['target' => 'お支払い方法の削除']));
    }
}
