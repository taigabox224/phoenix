<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Cashier\Billable;


/**
 * Class User
 * @package App\Models
 */
class User extends Authenticatable
{
    use Notifiable;
    use Billable;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime'
    ];

    /**
     * クレジットカードまたは銀行振込にて支払済み
     * @return bool
     */
    public function isPaid()
    {
        return $this->isPaidByBankTransfer();
    }

    /**
     * 銀行振込にて支払済み
     * @return bool
     */
    public function isPaidByBankTransfer()
    {
        $payment_status = $this->payment_status_bank;
        return $payment_status == 1 || $payment_status == 3;
    }
}
