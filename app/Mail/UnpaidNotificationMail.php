<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class UnpaidNotificationMail extends Mailable
{
    use Queueable, SerializesModels;

    private $bcc_address = 'taigabox224@gmail.com';

    public $user;

    /**
     * UnpaidNotificationMail constructor.
     * @param $user
     */
    public function __construct($user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->subject('フェニックス継続のお支払いについて') // メールタイトル
            ->bcc($this->bcc_address)
            ->text('mail.unpaid_notification') // メール本文のテンプレートとなるviewを設定
            ->with(['name' => $this->user->name]);
    }
}
