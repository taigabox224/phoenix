<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::prefix('home')->name('home.')->group(function(){
    Route::get('/', 'HomeController@index')->name('index');
    Route::post('download', 'HomeController@download')->name('download');
});

Route::namespace('Payment')->prefix('payment')->name('payment.')->group(function(){
    Route::get('/', 'PaymentController@index')->name('index');
    Route::post('subscribe', 'PaymentController@subscribe')->name('subscribe');
    Route::post('update', 'PaymentController@update')->name('update');
    Route::post('cancel', 'PaymentController@cancel')->name('cancel');
});

Route::post('/stripe/webhook');
