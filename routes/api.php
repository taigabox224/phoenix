<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::namespace('User')->prefix('user')->name('user.')->group(function(){
    Route::post('/', 'UserAPIController@user')->name('index');
    Route::post('collection', 'UserAPIController@userCollection')->name('userCollection');
    Route::post('search', 'UserAPIController@userSearch')->name('userSearch');
    Route::post('update', 'UserAPIController@update')->name('update');
    Route::post('destroy', 'UserAPIController@destroy')->name('destroy');
    Route::post('collection/archived', 'UserAPIController@getArchivedList')->name('getArchivedList');
});
